package br.com.ponto.controller;


import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ponto.dto.MarcadorHoraDTO;
import br.com.ponto.enums.PeriodoEnum;
import br.com.ponto.error.ResourceForbiddenException;
import br.com.ponto.model.FolhaPonto;
import br.com.ponto.model.Marcacao;
import br.com.ponto.repository.FolhaPontoRepository;
import br.com.ponto.repository.MarcacaoRepository;

/**
 * @author Rodrigo Carlos Cardoso - 20/01/2022 08:29:26
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class FolhaPontoControllerTest {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@MockBean
	private FolhaPontoRepository folhaPontoRepository; 

	@MockBean
	private MarcacaoRepository marcacaoRepository;
	
	@Rule
	public ExpectedException thorwn = ExpectedException.none();
	
	@Test
	public void postDataNaoInformadaStatus400 () {
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", new MarcadorHoraDTO(""), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(400);
		Assertions.assertThat(response.getBody()).contains("mensagem","Campo obrigatório não informado");
	}
	
	@Test
	public void postDataFormatoInvalidoStatus400 () {
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", new MarcadorHoraDTO("2018"), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(400);
		Assertions.assertThat(response.getBody()).contains("mensagem","Data e hora em formato inválido");
	}
	
	@Test
	public void postDataFormatoInvalido2Status400 () {
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", new MarcadorHoraDTO("2018-08-22"), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(400);
		Assertions.assertThat(response.getBody()).contains("mensagem","Data e hora em formato inválido");
	}
	
	@Test
	public void postDataFormatoInvalido3Status400 () {
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", new MarcadorHoraDTO("a"), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(400);
		Assertions.assertThat(response.getBody()).contains("mensagem","Data e hora em formato inválido");
	}
	
	@Test
	public void postSabadoNaoPermitidoStatus403 () {
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", new MarcadorHoraDTO("2021-06-12T08:00:00"), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(403);
		Assertions.assertThat(response.getBody()).contains("mensagem","Sábado e domingo não são permitidos como dia de trabalho");
	}
	
	@Test
	public void postDomingoNaoPermitidoStatus403 () {
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", new MarcadorHoraDTO("2021-06-13T08:00:00"), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(403);
		Assertions.assertThat(response.getBody()).contains("mensagem","Sábado e domingo não são permitidos como dia de trabalho");
	}
	
	@Test
	public void postHorarioJaRegistradoStatus409 () {
		MarcadorHoraDTO marcadorHoraDTO = new MarcadorHoraDTO("2021-06-14T08:00:00");
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		BDDMockito.when(folhaPontoRepository.recuperarFolhaPontoPorDiaFolhaPonto(marcadorHoraDTO.getDataHoraCalendar())).thenReturn(criarFolhaPonto());
		BDDMockito.when( marcacaoRepository.recuperarMarcaoPorFolhaPonto(criarFolhaPonto())).thenReturn(criarMarcacoes());
		
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", marcadorHoraDTO, String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(409);
		Assertions.assertThat(response.getBody()).contains("mensagem","Horários já registrado");
	}
	
	@Test
	public void postHorarioAnteriorUltimoLancamentoStatus409 () {
		MarcadorHoraDTO marcadorHoraDTO = new MarcadorHoraDTO("2021-06-14T07:00:00");
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		BDDMockito.when(folhaPontoRepository.recuperarFolhaPontoPorDiaFolhaPonto(marcadorHoraDTO.getDataHoraCalendar())).thenReturn(criarFolhaPonto());
		BDDMockito.when( marcacaoRepository.recuperarMarcaoPorFolhaPonto(criarFolhaPonto())).thenReturn(criarMarcacoes());
		
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", marcadorHoraDTO, String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(409);
		Assertions.assertThat(response.getBody()).contains("mensagem","Hora não pode ser anterior ao último lançamento anterior");
	}
	
	@Test
	public void post4HorariosApenasStatus403 () {
		MarcadorHoraDTO marcadorHoraDTO = new MarcadorHoraDTO("2021-06-14T12:00:00");
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		BDDMockito.when(folhaPontoRepository.recuperarFolhaPontoPorDiaFolhaPonto(marcadorHoraDTO.getDataHoraCalendar())).thenReturn(criarFolhaPonto());
		BDDMockito.when( marcacaoRepository.recuperarMarcaoPorFolhaPonto(criarFolhaPonto())).thenReturn(criar4Marcacoes());
		
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", marcadorHoraDTO, String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(403);
		Assertions.assertThat(response.getBody()).contains("mensagem","Apenas 4 horários podem ser registrados por dia");
	}
	
	
	@Test
	public void post1HoraAlmocoStatus403 () {
		MarcadorHoraDTO marcadorHoraDTO = new MarcadorHoraDTO("2021-06-14T09:59:59");
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		BDDMockito.when(folhaPontoRepository.recuperarFolhaPontoPorDiaFolhaPonto(marcadorHoraDTO.getDataHoraCalendar())).thenReturn(criarFolhaPonto());
		BDDMockito.when( marcacaoRepository.recuperarMarcaoPorFolhaPonto(criarFolhaPonto())).thenReturn(criarMarcacoesAlmoco());
		
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", marcadorHoraDTO, String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(403);
		Assertions.assertThat(response.getBody()).contains("mensagem","Deve haver no mínimo 1 hora de almoço");
	}
	
	@Test
	public void postNovoDiaStatus201 () {
		MarcadorHoraDTO marcadorHoraDTO = new MarcadorHoraDTO("2021-06-14T09:59:59");
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		BDDMockito.when(folhaPontoRepository.recuperarFolhaPontoPorDiaFolhaPonto(marcadorHoraDTO.getDataHoraCalendar())).thenReturn(null);
		BDDMockito.when( marcacaoRepository.recuperarMarcaoPorFolhaPonto(criarFolhaPonto())).thenReturn(criarMarcacoesAlmoco());
		
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", marcadorHoraDTO, String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}

	@Test
	public void postNovaMarcvaoStatus201 () {
		MarcadorHoraDTO marcadorHoraDTO = new MarcadorHoraDTO("2021-06-14T10:00:00");
		BDDMockito.when(folhaPontoRepository.save(criarFolhaPonto())).thenReturn(criarFolhaPonto());
		BDDMockito.when(folhaPontoRepository.recuperarFolhaPontoPorDiaFolhaPonto(marcadorHoraDTO.getDataHoraCalendar())).thenReturn(criarFolhaPonto());
		BDDMockito.when( marcacaoRepository.recuperarMarcaoPorFolhaPonto(criarFolhaPonto())).thenReturn(criarMarcacoesAlmoco());
		
		ResponseEntity<String> response = restTemplate.postForEntity("/batidas", marcadorHoraDTO, String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}
	
	private Marcacao criarMarcacao(String marcacaoHorario, PeriodoEnum periodo) {
		Marcacao marcacao = new Marcacao();
		marcacao.setHoraMarcacao(criarHora(marcacaoHorario));
		marcacao.setPeriodo(periodo);
		return marcacao;
	}
	
	private List<Marcacao> criarMarcacoes() {
		List<Marcacao> lista = new ArrayList<Marcacao>();
		lista.add(criarMarcacao("2021-06-14T08:00:00", PeriodoEnum.HORA_INICIO_PRIMEIRO_TURNO));
		return lista;
	}
	
	private List<Marcacao> criar4Marcacoes() {
		List<Marcacao> lista = new ArrayList<Marcacao>();
		lista.add(criarMarcacao("2021-06-14T08:00:00", PeriodoEnum.HORA_INICIO_PRIMEIRO_TURNO));
		lista.add(criarMarcacao("2021-06-14T09:00:00", PeriodoEnum.HORA_FIM_PRIMEIRO_TURNO));
		lista.add(criarMarcacao("2021-06-14T10:00:00", PeriodoEnum.HORA_INICIO_SEGUNDO_TURNO));
		lista.add(criarMarcacao("2021-06-14T11:00:00", PeriodoEnum.HORA_FIM_SEGUNDO_TURNO));
		return lista;
	}
	
	private List<Marcacao> criarMarcacoesAlmoco() {
		List<Marcacao> lista = new ArrayList<Marcacao>();
		lista.add(criarMarcacao("2021-06-14T08:00:00", PeriodoEnum.HORA_INICIO_PRIMEIRO_TURNO));
		lista.add(criarMarcacao("2021-06-14T09:00:00", PeriodoEnum.HORA_FIM_PRIMEIRO_TURNO));
		return lista;
	}
	
	private FolhaPonto criarFolhaPonto() {
		FolhaPonto ponto = new FolhaPonto();
		ponto.setDiaFolhaPonto(criarData("2021-06-16T08:00:00"));
		return ponto;
	}
	
	private Calendar criarData(String strDate)  {
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	    Calendar c = Calendar.getInstance();     
	    try {
			c.setTime(sdf.parse(strDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    return c;
	}
	
	
	public Time criarHora(String dataHora) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		try {
			if(!StringUtils.isEmpty(dataHora) && dataHora.length() > 11)
					return new Time(sdf.parse(dataHora.substring(11)).getTime());
		} catch (ParseException e) {
			throw new ResourceForbiddenException("Data e hora em formato inválido.");
		}
		return null;
	}
	
	
	

}
