package br.com.ponto.repository;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ponto.model.FolhaPonto;

/**
 * @author Rodrigo Carlos Cardoso - 18/01/2022 13:42:22
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class FolhaPontoRepositoryTest {
	
	@Autowired
	private FolhaPontoRepository repository;
		
	@Rule
	public ExpectedException thorwn = ExpectedException.none();
	
	@Test
	public void salvarTeste () {
		FolhaPonto ponto = criarFolhaPonto();
		
		repository.save(ponto);
		Assertions.assertThat(ponto.getId()).isNotNull();
		Assertions.assertThat(ponto.getDiaFolhaPonto().getTime().getTime()).isEqualTo(criarData("01/07/1990").getTime().getTime());
	}
	
	@Test
	public void recuperarFolhaPontoPorDiaFolhaPontoTeste () {
		FolhaPonto ponto = criarFolhaPonto();
		
		repository.save(ponto);
		
		FolhaPonto recuperado = repository.recuperarFolhaPontoPorDiaFolhaPonto(ponto.getDiaFolhaPonto());
		
		Assertions.assertThat(recuperado.getId()).isNotNull();
		Assertions.assertThat(recuperado.getId()).isEqualTo(ponto.getId());
		Assertions.assertThat(recuperado.getDiaFolhaPonto().getTime().getTime()).isEqualTo(ponto.getDiaFolhaPonto().getTime().getTime());
	}

	private FolhaPonto criarFolhaPonto() {
		FolhaPonto ponto = new FolhaPonto();
		ponto.setDiaFolhaPonto(criarData("01/07/1990"));
		return ponto;
	}
	
	private Calendar criarData(String strDate)  {
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	    Calendar c = Calendar.getInstance();     
	    try {
			c.setTime(sdf.parse(strDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    return c;
	}

}
