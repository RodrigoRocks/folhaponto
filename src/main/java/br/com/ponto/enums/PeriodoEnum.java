package br.com.ponto.enums;

import br.com.ponto.error.ResourceForbiddenException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PeriodoEnum {
	
	HORA_INICIO_PRIMEIRO_TURNO(1, "HORA_INICIO_PRIMEIRO_TURNO"),
	HORA_FIM_PRIMEIRO_TURNO(2, "HORA_FIM_PRIMEIRO_TURNO"),
	HORA_INICIO_SEGUNDO_TURNO(3, "HORA_INICIO_SEGUNDO_TURNO"),
	HORA_FIM_SEGUNDO_TURNO(4, "HORA_FIM_SEGUNDO_TURNO");
	
	private Integer ordem;
	private String descricao;
	
	public static PeriodoEnum recuperarProximoPeriodo(Integer quatidadePeriodo) {
		quatidadePeriodo++;
		
		PeriodoEnum[] values = values();
		for (int i = 0; i < values.length; i++) {
			PeriodoEnum valor = values[i];
			if (valor.ordem == quatidadePeriodo) {
				return valor;
			}
		}
		
		throw new ResourceForbiddenException("Apenas 4 horários podem ser registrados por dia");
		
	}
}
