package br.com.ponto.service;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ponto.dto.MarcadorHoraDTO;
import br.com.ponto.enums.PeriodoEnum;
import br.com.ponto.error.ResourceConflictException;
import br.com.ponto.error.ResourceForbiddenException;
import br.com.ponto.model.FolhaPonto;
import br.com.ponto.model.Marcacao;
import br.com.ponto.repository.FolhaPontoRepository;
import br.com.ponto.repository.MarcacaoRepository;
import br.com.ponto.util.DataUtil;

@Service
public class FolhaPontoService {
	
	private static final Logger LOG = LoggerFactory.getLogger(FolhaPontoService.class);
	
	@Autowired
	private FolhaPontoRepository repository;
	
	@Autowired
	private MarcacaoRepository marcacaoRepository;
	
	@Transactional
	public FolhaPonto salvar(MarcadorHoraDTO marcadorHoraDTO) {

		LOG.info("Obtendo folha de ponto do dia {}", marcadorHoraDTO.getDataHora());
		FolhaPonto folhaPonto = repository.recuperarFolhaPontoPorDiaFolhaPonto(marcadorHoraDTO.getDataHoraCalendar());

		folhaPonto = salvar(marcadorHoraDTO, folhaPonto);

		LOG.info("Salvando folha de ponto do dia {}", marcadorHoraDTO.getDataHora());
		
		return folhaPonto;
	}

	private FolhaPonto salvar(MarcadorHoraDTO marcadorHoraDTO, FolhaPonto folhaPonto) {
		if(folhaPonto == null) {
			validarSabadoDomingo(marcadorHoraDTO);
			folhaPonto = new FolhaPonto().paraDia(marcadorHoraDTO.getDataHoraCalendar());
			repository.save(folhaPonto);
			 
			folhaPonto.getMarcacoes().add(marcacaoRepository.save(new Marcacao(folhaPonto, 
				PeriodoEnum.HORA_INICIO_PRIMEIRO_TURNO,
				marcadorHoraDTO.getHora())));
		}else {
			List<Marcacao> macacoes = marcacaoRepository.recuperarMarcaoPorFolhaPonto(folhaPonto);
			validarFolhaPonto(macacoes, marcadorHoraDTO);
			
			folhaPonto.getMarcacoes().add(marcacaoRepository.save(new Marcacao(folhaPonto, 
				PeriodoEnum.recuperarProximoPeriodo(macacoes.size()),
				marcadorHoraDTO.getHora())));
		}
		return folhaPonto;
	}

	private void validarFolhaPonto(List<Marcacao> marcacoes, MarcadorHoraDTO marcadorHoraDTO) {
		
		//409
		validarHorarioJaRegistrado(marcacoes, marcadorHoraDTO);
		validarHorarioAnterior(marcacoes, marcadorHoraDTO);
		
		//403
		validarSabadoDomingo(marcadorHoraDTO);
		validarUmaHoraAlmoco(marcacoes, marcadorHoraDTO);
		
	}
	
	private void validarSabadoDomingo( MarcadorHoraDTO marcadorHoraDTO) {
		if(DataUtil.checarFDS(marcadorHoraDTO.getDataHoraCalendar())) {
			throw new ResourceForbiddenException("Sábado e domingo não são permitidos como dia de trabalho");
		}
	}

	private void validarHorarioJaRegistrado(List<Marcacao> marcacoes, MarcadorHoraDTO marcadorHoraDTO) {
		if(marcacoes.stream().anyMatch(s ->s.getHoraMarcacao().getTime() == marcadorHoraDTO.getHora().getTime())) {
			throw new ResourceConflictException("Horários já registrado");
		}
	}

	private void validarUmaHoraAlmoco(List<Marcacao> marcacoes, MarcadorHoraDTO marcadorHoraDTO) {
		List<Marcacao> lista = marcacoes.stream().
				filter((s -> s.getPeriodo().equals(PeriodoEnum.HORA_FIM_PRIMEIRO_TURNO))).collect(Collectors.toList());
		if(!lista.isEmpty()) {
			Calendar marcacaoFimPrimeiro = Calendar.getInstance();
			marcacaoFimPrimeiro.setTime(lista.get(0).getHoraMarcacao());
			marcacaoFimPrimeiro.add(Calendar.HOUR_OF_DAY, 1);
			if(marcacaoFimPrimeiro.getTime().getTime() > marcadorHoraDTO.getHora().getTime())
				throw new ResourceForbiddenException("Deve haver no mínimo 1 hora de almoço");
		}
		
	}
	
	
	private void validarHorarioAnterior(List<Marcacao> marcacoes, MarcadorHoraDTO marcadorHoraDTO) {
		if(marcacoes.get(marcacoes.size()-1).getHoraMarcacao().getTime() > marcadorHoraDTO.getHora().getTime())
			throw new ResourceConflictException("Hora não pode ser anterior ao último lançamento anterior");
	}
}
