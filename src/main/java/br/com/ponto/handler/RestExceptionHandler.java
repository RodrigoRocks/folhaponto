package br.com.ponto.handler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.ponto.error.ResourceConflictDetails;
import br.com.ponto.error.ResourceConflictException;
import br.com.ponto.error.ResourceForbiddenDetails;
import br.com.ponto.error.ResourceForbiddenException;
import br.com.ponto.error.ValidationErrorDetails;

@ControllerAdvice
public class RestExceptionHandler {
	
	@ExceptionHandler(ResourceForbiddenException.class)
	public ResponseEntity<?> handleResourceForbiddenException(ResourceForbiddenException rfException){
		ResourceForbiddenDetails rfDetails = ResourceForbiddenDetails.builder()
		.mensagem(rfException.getMessage()).build();
		
		return new ResponseEntity<>(rfDetails, HttpStatus.FORBIDDEN);
		
	}
	
	@ExceptionHandler(ResourceConflictException.class)
	public ResponseEntity<?> handleResourceForbiddenException(ResourceConflictException rcException){
		ResourceConflictDetails rcDetails = ResourceConflictDetails.builder()
		.mensagem(rcException.getMessage()).build();
		
		return new ResponseEntity<>(rcDetails, HttpStatus.CONFLICT);
		
	}
	
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException manvException){
		
		List<FieldError> listaErro = manvException.getBindingResult().getFieldErrors();
		listaErro.stream().map(fr -> {
			return fr.getDefaultMessage();
		}).collect(Collectors.joining(","));
		ValidationErrorDetails rfDetails = ValidationErrorDetails.builder()
		.mensagem(listaErro.stream().map(fr -> {
			return fr.getDefaultMessage();
		}).collect(Collectors.joining(","))).build();
		
		return new ResponseEntity<>(rfDetails, HttpStatus.BAD_REQUEST);
		
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException manvException){
		
		ValidationErrorDetails rfDetails = ValidationErrorDetails.builder()
		.mensagem("Campo obrigatório não informado").build();
		return new ResponseEntity<>(rfDetails, HttpStatus.BAD_REQUEST);
		
	}
	
	
	

}



