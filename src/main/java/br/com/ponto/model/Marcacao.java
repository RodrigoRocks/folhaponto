package br.com.ponto.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.ponto.enums.PeriodoEnum;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "MARCACAO")
public class Marcacao extends AbstractEntity{
    
	private static final long serialVersionUID = -8496010261260580519L;

	@ManyToOne()
	@JoinColumn(name = "ID_FOLHA_PONTO", referencedColumnName = "ID", nullable = false)
    @JsonIgnore
	private FolhaPonto folhaPonto;
	
	@Enumerated(EnumType.ORDINAL)
    @Column(name = "CD_PERIODO")
    private PeriodoEnum periodo;
	
	@Temporal(TemporalType.TIME) 
	@Column(name = "HORA_MARCACAO")
	private Date horaMarcacao;
   
    @Override
    public String toString() {
        return "marcacao {" +
                "id=" + getId() +
                ", periodo='" + (periodo != null ? periodo.getDescricao() : null) + '\'' +
                ", dia='" + horaMarcacao + '\'' +
                '}';
    }

	public Marcacao(FolhaPonto folhaPonto, PeriodoEnum periodo, Date horaMarcacao) {
		super();
		this.folhaPonto = folhaPonto;
		this.periodo = periodo;
		this.horaMarcacao = horaMarcacao;
	}
	
	public Marcacao() {
		super();
	}
    
    

}
