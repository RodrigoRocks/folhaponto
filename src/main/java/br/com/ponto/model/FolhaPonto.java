package br.com.ponto.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Rodrigo Carlos Cardoso 18/01/2022
 *
 */
@Entity
@Table(name = "FOLHA_PONTO")
@Getter
@Setter
public class FolhaPonto extends AbstractEntity{
	
	private static final long serialVersionUID = -3195521163941271484L;
	
	@Temporal(TemporalType.DATE)
    @Column(name = "DIA_FOLHA_PONTO", nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Calendar diaFolhaPonto;
   
    @OneToMany(fetch=FetchType.LAZY, mappedBy="folhaPonto")
	@OrderBy("periodo")
	private List<Marcacao> marcacoes = new ArrayList<>(0);
    
    public FolhaPonto paraDia(Calendar dia) {
		this.diaFolhaPonto = dia;
		return this;
	}
   
    @Override
    public String toString() {
        return "folhaPonto {" +
                "id=" + getId() +
                ", dia='" + getDiaFolhaPonto()  + '\'' +
                ", quantidade='" + getMarcacoes().size() + '\'' +
                '}';
    }

}
