package br.com.ponto.dto;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.ponto.error.ResourceForbiddenException;
import br.com.ponto.validation.ValidDate;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarcadorHoraDTO {
	
	@NotEmpty(message = "Campo obrigatório não informado")
	@ValidDate
	private String dataHora;
	
	@JsonIgnore
	public Calendar getDataHoraCalendar() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			if(!StringUtils.isEmpty(dataHora))
				cal.setTime(sdf.parse(dataHora));
		} catch (ParseException e) {
			throw new ResourceForbiddenException("Data e hora em formato inválido.");
		}
		return cal;
	}
	
	@JsonIgnore
	public Time getHora() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		try {
			if(!StringUtils.isEmpty(dataHora) && dataHora.length() > 11)
					return new Time(sdf.parse(dataHora.substring(11)).getTime());
		} catch (ParseException e) {
			throw new ResourceForbiddenException("Data e hora em formato inválido.");
		}
		return null;
	}

	public MarcadorHoraDTO(String dataHora) {
		super();
		this.dataHora = dataHora;
	}
	
	public MarcadorHoraDTO() {
		super();
	}
}
