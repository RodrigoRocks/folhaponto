package br.com.ponto.repository;

import java.util.Calendar;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ponto.model.FolhaPonto;

@Repository
public interface FolhaPontoRepository  extends JpaRepository<FolhaPonto, Long>{
	
	@Query("from FolhaPonto fp where fp.diaFolhaPonto = :diaFolhaPonto")
	FolhaPonto recuperarFolhaPontoPorDiaFolhaPonto(@Param("diaFolhaPonto") Calendar diaFolhaPonto);
	
	@Query("from FolhaPonto fp join FETCH fp.marcacoes where fp.diaFolhaPonto = :diaFolhaPonto")
	FolhaPonto recuperarFolhaPontoPorDiaFolhaPontoFetchMarcacoes(@Param("diaFolhaPonto") Calendar diaFolhaPonto);
}
