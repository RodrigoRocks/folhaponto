package br.com.ponto.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ponto.model.FolhaPonto;
import br.com.ponto.model.Marcacao;

@Repository
public interface MarcacaoRepository  extends JpaRepository<Marcacao, Long>{
	
	@Query("from Marcacao m  where m.folhaPonto = :folhaPonto")
	List<Marcacao> recuperarMarcaoPorFolhaPonto(@Param("folhaPonto") FolhaPonto folhaPonto);
	
}
