package br.com.ponto.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ponto.dto.MarcadorHoraDTO;
import br.com.ponto.service.FolhaPontoService;

@RestController
@RequestMapping("/batidas")
public class FolhaPontoController {
	
	@Autowired
	private FolhaPontoService folhaPontoService; 
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> marcarHora(@Valid @RequestBody(required = true) MarcadorHoraDTO dia) {
		return new ResponseEntity(folhaPontoService.salvar(dia),HttpStatus.CREATED);
	}

}
