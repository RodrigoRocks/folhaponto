package br.com.ponto.error;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
public class ErrorDetails {
	private String mensagem;
}
