# Prova Backend Spring Boot v4 (Web API para controle de ponto)

Autor: Rodrigo Carlos Cardoso
Data: 18/01/2022

### Finalidade:
Registrar um horário da jornada diária de trabalho

### Check list:
✓É mais importante qualidade de código do que o desafio 100% feito.

✓ Se não der tempo de fazer todas as funcionalidades, não tem problema. A ideia é
validar a solução de problemas, qualidade do código, organização, testes, arquitetura e
etc.

✓ Siga o contrato Open API. Atente-se ao formato de mensagens de input, response e
erros.

✓ Utilize Java 11

✓ Utilize Springboot

✓ Não utilize implementações como jHipster

✓ Lembre-se dos testes unitários



### Pré requisitos

1. Java 11
2. Maven/Gradle
3. JPA
4. Spring Boot
5. Spring Data
6. H2database
2. IDE
3. Postman


### Instalando o back-end

1. Importar os projetos para uma IDE (Eclipse| Intellij | Visual Studio Code Com extensões para Java)
2. Iniciar os projetos na seguinte ordem:
  
```
mvn clean install

```
3. Executar PontoApplication.java como Spring Boot App.

## Documentações de referência para bibliotecas utilizadas

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/maven-plugin/reference/html/)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/reference/htmlsingle/#using-boot-devtools)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [H2database](http://www.h2database.com/html/features.html)

## Acessar H2

URL de acesso:
* [http://localhost:8080/h2-console/](http://www.h2database.com/html/features.html)

Dados de conexão estão no arquivo application.yml


## Validações

1. Campo obrigatório não informado [400]
2. Data e hora em formato inválido [400]
3. Sábado e domingo não são permitidos como dia de trabalho [403]
4. Horários já registrado [409]
5. Apenas 4 horários podem ser registrados por dia [403]
6. Deve haver no mínimo 1 hora de almoço [403]
7. Hora não pode ser anterior ao último lançamento anterior [409]

## Contrato Open API

```
openapi: "3.0.3"
info:
  title: Controle de Ponto API
  version: "1.0"
components:
  schemas:
    momento:
      type: object
      description: O momento da batida
      properties:
        dataHora:
          description: Data e hora da batida
          type: string
          example: "2018-08-22T08:00:00"
    mensagem:
      type: object
      properties:
        mensagem:
          type: string
paths:
  /batidas:
    post:
      summary: Bater ponto
      description: |
        Registrar um horário da jornada diária de trabalho
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/momento'
      responses:
        "201":
          description: |
            Created
        "400":
          description: |
            Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/mensagem'
              examples:
                Data e hora em formato inválido:
                  value:
                    mensagem: Data e hora em formato inválido
                Campo obrigatório não informado:
                  value:
                    mensagem: Campo obrigatório não informado
        "403":
          description: |
            Forbiden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/mensagem'
              examples:
                apenas 4 horários por dia:
                  value:
                    mensagem: Apenas 4 horários podem ser registrados por dia
                mínimo 1 hora de almoço:
                  value:
                    mensagem: Deve haver no mínimo 1 hora de almoço
                sábado e domingo não são permitidos:
                  value:
                    mensagem: Sábado e domingo não são permitidos como dia de trabalho
        "409":
          description: |
            Conflict
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/mensagem'
              examples:
                Horários já registrado:
                  value:
                    mensagem: Horários já registrado

```

## Collection Postman

```
{
	"info": {
		"_postman_id": "0863c3bd-d24b-4746-bc12-0210a541ada3",
		"name": "folha-de-ponto",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "batidas",
			"item": [
				{
					"name": "batidas | campo obrigatório",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Campo obrigatório não informado\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(400);\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas | campo obrigatório JSON vazio",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Campo obrigatório não informado\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(400);\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas | data em formato inválido 3",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Data e hora em formato inválido\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(400);\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"dataHora\": \"2018\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas | data em formato inválido 1",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Data e hora em formato inválido\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(400);\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"dataHora\": \"2018-08-22\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas | data em formato inválido 2",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Data e hora em formato inválido\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(400);\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"dataHora\": \"a\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas | sábado não é permitido",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Sábado e domingo não são permitidos como dia de trabalho\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(403);\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"dataHora\": \"2021-06-12T08:00:00\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas | domingo não é permitido",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Sábado e domingo não são permitidos como dia de trabalho\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(403);\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"dataHora\": \"2021-06-13T08:00:00\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas | horário já registrado",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Horário já registrado\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(409);\r",
									"});"
								],
								"type": "text/javascript"
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									"pm.sendRequest({\r",
									"    url: \"http://localhost:8080/batidas\",\r",
									"    method: \"POST\",\r",
									"    header: {\r",
									"        \"Content-Type\": \"application/json\"\r",
									"    },\r",
									"    body: {\r",
									"        mode: \"raw\",\r",
									"        raw: JSON.stringify({\r",
									"            \"dataHora\": \"2021-06-14T08:00:00\"\r",
									"        })\r",
									"    }\r",
									"}, function (err, res) {\r",
									"\r",
									"    setTimeout(function () {\r",
									"        console.log(\"Sleeping for 3 seconds before next request.\");\r",
									"    }, 3000);\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"dataHora\": \"2021-06-14T08:00:00\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas | apenas 4 horários",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Apenas 4 horários podem ser registrados por dia\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(403);\r",
									"});"
								],
								"type": "text/javascript"
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									"pm.sendRequest({\r",
									"    url: \"http://localhost:8080/batidas\",\r",
									"    method: \"POST\",\r",
									"    header: {\r",
									"        \"Content-Type\": \"application/json\"\r",
									"    },\r",
									"    body: {\r",
									"        mode: \"raw\",\r",
									"        raw: JSON.stringify({\r",
									"            \"dataHora\": \"2021-06-17T08:00:00\"\r",
									"        })\r",
									"    }\r",
									"}, function (err, res) {\r",
									"\r",
									"    pm.sendRequest({\r",
									"        url: \"http://localhost:8080/batidas\",\r",
									"        method: \"POST\",\r",
									"        header: {\r",
									"            \"Content-Type\": \"application/json\"\r",
									"        },\r",
									"        body: {\r",
									"            mode: \"raw\",\r",
									"            raw: JSON.stringify({\r",
									"                \"dataHora\": \"2021-06-17T12:00:00\"\r",
									"            })\r",
									"        }\r",
									"    }, function (err, res) {\r",
									"\r",
									"        pm.sendRequest({\r",
									"            url: \"http://localhost:8080/batidas\",\r",
									"            method: \"POST\",\r",
									"            header: {\r",
									"                \"Content-Type\": \"application/json\"\r",
									"            },\r",
									"            body: {\r",
									"                mode: \"raw\",\r",
									"                raw: JSON.stringify({\r",
									"                    \"dataHora\": \"2021-06-17T13:00:00\"\r",
									"                })\r",
									"            }\r",
									"        }, function (err, res) {\r",
									"            pm.sendRequest({\r",
									"                url: \"http://localhost:8080/batidas\",\r",
									"                method: \"POST\",\r",
									"                header: {\r",
									"                    \"Content-Type\": \"application/json\"\r",
									"                },\r",
									"                body: {\r",
									"                    mode: \"raw\",\r",
									"                    raw: JSON.stringify({\r",
									"                        \"dataHora\": \"2021-06-17T18:00:00\"\r",
									"                    })\r",
									"                }\r",
									"            }, function (err, res) {\r",
									"\r",
									"                setTimeout(function () {\r",
									"                    console.log(\"Sleeping for 3 seconds before next request.\");\r",
									"                }, 3000);\r",
									"            });\r",
									"        });\r",
									"    });\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"dataHora\": \"2021-06-17T20:00:00\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas | mínimo 1h de almoço",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"response value\", function () {\r",
									"    var expected_json = {\r",
									"        \"mensagem\": \"Deve haver no mínimo 1 hora de almoço\"\r",
									"    };\r",
									"\r",
									"    pm.expect(pm.response.text()).to.be.equal(JSON.stringify(expected_json))\r",
									"});\r",
									"\r",
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(403);\r",
									"});"
								],
								"type": "text/javascript"
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									"pm.sendRequest({\r",
									"    url: \"http://localhost:8080/batidas\",\r",
									"    method: \"POST\",\r",
									"    header: {\r",
									"        \"Content-Type\": \"application/json\"\r",
									"    },\r",
									"    body: {\r",
									"        mode: \"raw\",\r",
									"        raw: JSON.stringify({\r",
									"            \"dataHora\": \"2021-06-16T08:00:00\"\r",
									"        })\r",
									"    }\r",
									"}, function (err, res) {\r",
									"\r",
									"    pm.sendRequest({\r",
									"        url: \"http://localhost:8080/batidas\",\r",
									"        method: \"POST\",\r",
									"        header: {\r",
									"            \"Content-Type\": \"application/json\"\r",
									"        },\r",
									"        body: {\r",
									"            mode: \"raw\",\r",
									"            raw: JSON.stringify({\r",
									"                \"dataHora\": \"2021-06-16T12:00:00\"\r",
									"            })\r",
									"        }\r",
									"    }, function (err, res) {\r",
									"\r",
									"        setTimeout(function () {\r",
									"            console.log(\"Sleeping for 3 seconds before next request.\");\r",
									"        }, 3000);\r",
									"    });\r",
									"});"
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"dataHora\": \"2021-06-16T12:59:00\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				},
				{
					"name": "batidas",
					"event": [
						{
							"listen": "test",
							"script": {
								"exec": [
									"pm.test(\"status code\", function () {\r",
									"    pm.expect(pm.response.code).to.be.equal(201);\r",
									"});"
								],
								"type": "text/javascript"
							}
						},
						{
							"listen": "prerequest",
							"script": {
								"exec": [
									""
								],
								"type": "text/javascript"
							}
						}
					],
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"dataHora\": \"2021-06-21T01:00:00\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/batidas",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"batidas"
							]
						}
					},
					"response": []
				}
			]
		}
	]
}
```

